﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace liveChatImporter
{
    class Program
    {
        static TextWriter tx;
        static void Main(string[] args)
        {

            var FechaHoy = DateTime.Now.ToString("yyyy-MM-dd");
            //var Fechadesde = "2019-08-02";
            //var operatorName = "Gladys Rodriguez";


            string pathLog = AppDomain.CurrentDomain.BaseDirectory + FechaHoy + "log.txt";
            tx = new StreamWriter(pathLog, true);

            int pagesCount = 0;
            int currentPage = 1;
            try
            {

                string liveChatUsername = System.Configuration.ConfigurationManager.AppSettings["liveChatUsername"];
                string liveChatApiKey = System.Configuration.ConfigurationManager.AppSettings["liveChatApiKey"];
                string liveChatUrl = System.Configuration.ConfigurationManager.AppSettings["liveChatUrl"];

                //Primera ejecucion al api
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var byteArray = Encoding.ASCII.GetBytes(liveChatUsername + ":" + liveChatApiKey);
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                Uri uri = new Uri(liveChatUrl + "/chats?date_to=" + FechaHoy + "&page=" + currentPage.ToString());
                var response = client.GetStringAsync(uri);
                response.Wait();
                var result = response.Result;

                //Lista de variables a usar
                List<Chat> listOfChat = new List<Chat>();
                RootObject totalResponse = JsonConvert.DeserializeObject<RootObject>(result);
                pagesCount = totalResponse.pages;
                listOfChat.AddRange(totalResponse.chats);

                //Ciclo por si es mas de 1 pagina
                while (pagesCount > 1)
                {
                    pagesCount--;
                    currentPage++;

                    Console.WriteLine("Pagina actual " + currentPage + " quedan " + pagesCount);

                    uri = new Uri(liveChatUrl + "/chats?date_to=" + FechaHoy + "&page=" + currentPage.ToString());
                    response = client.GetStringAsync(uri);
                    response.Wait();
                    result = response.Result;
                    totalResponse = JsonConvert.DeserializeObject<RootObject>(result);

                    listOfChat.AddRange(totalResponse.chats);
                }

                //Recorremos cada chat
                foreach (var chat in listOfChat)
                {
                    //Se guarda el chat, los mensajes y las encuestas

                    CultureInfo provider = CultureInfo.InvariantCulture;
                    DateTime dateStart = DateTime.ParseExact(chat.started.Split(',')[1], "MM/dd/yy hh:mm:ss tt", provider, DateTimeStyles.AllowLeadingWhite);
                    DateTime dateEnd = DateTime.ParseExact(chat.ended.Split(',')[1], "MM/dd/yy hh:mm:ss tt", provider, DateTimeStyles.AllowLeadingWhite);

                    Console.WriteLine("Chat ID: " + chat.id + " con fecha: " + dateStart);

                    tx.WriteLine("Guardando chatId: " + chat.id);

                    using (FIDCRMModel db = new FIDCRMModel())
                    {
                        if (db.Chats.Where(b => b.chatId == chat.id).Any())
                        {
                            foreach (var operators in chat.operators)
                            {
                                if (db.Operators.Where(b => b.chatId == chat.id && b.operatorName == operators.display_name).Any())
                                {
                                    tx.WriteLine("No se guardo, estaba repetido todo el chat");
                                    continue;
                                }
                                else
                                {
                                    db.Operators.Add(new Operators
                                    {
                                        chatId = chat.id,
                                        operatorName = operators.display_name,
                                        operatorEmail = operators.email,
                                        operatorIp = operators.ip
                                    });
                                    db.SaveChanges();
                                    tx.WriteLine("No se guardo, estaba repetido el chat, si se guardo informacion de operador");
                                    continue;
                                }
                            }
                        }
                        else
                        {
                            if (chat.visitor_name != null || chat.visitor_name != "")
                            {
                                db.Chats.Add(new Chats
                                {
                                    chatId = chat.id,
                                    visitorName = chat.visitor == null ? (chat.visitor_name == null || chat.visitor_name == "" ? "N/E" : chat.visitor_name) : chat.visitor.name == null ? "" : chat.visitor.name,
                                    visitorId = chat.visitor == null ? (chat.visitor_id == null ? "" : chat.visitor_id) : chat.visitor.id,
                                    visitorIp = chat.visitor != null ? chat.visitor.ip : null,
                                    visitorCity = chat.visitor != null ? chat.visitor.city : null,
                                    visitorCountry = chat.visitor != null ? chat.visitor.country : null,
                                    visitorBrowserInfo = chat.visitor != null ? chat.visitor.user_agent : null,
                                    rate = chat.rate,
                                    duration = chat.duration,
                                    chatStartUrl = chat.chat_start_url,
                                    started = dateStart,
                                    ended = dateEnd
                                });


                                //Guarda cada operador
                                foreach (var operators in chat.operators)
                                {

                                    db.Operators.Add(new Operators
                                    {
                                        chatId = chat.id,
                                        operatorName = operators.display_name,
                                        operatorEmail = operators.email,
                                        operatorIp = operators.ip
                                    });
                                }

                                //Cada mensaje dentro del chat
                                foreach (var msj in chat.messages)
                                {

                                    DateTime msjDate = DateTime.ParseExact(msj.date.Split(',')[1], "MM/dd/yy hh:mm:ss tt", provider, DateTimeStyles.AllowLeadingWhite);

                                    db.Messages.Add(new Messages()
                                    {
                                        chatId = chat.id,
                                        authorName = msj.author_name,
                                        text = msj.text,
                                        date = msjDate,
                                        timestamp = msj.timestamp,
                                        agentId = msj.agent_id,
                                        userType = msj.user_type
                                    });
                                }

                                //Cada survey dentro de pre y post
                                foreach (var pre in chat.prechat_survey)
                                {
                                    db.Surveys.Add(new Surveys()
                                    {
                                        chatId = chat.id,
                                        surveyId = pre.id,
                                        surveyKey = pre.key,
                                        surveyType = "pre-survey",
                                        value = pre.value
                                    });
                                }
                                foreach (var post in chat.prechat_survey)
                                {
                                    db.Surveys.Add(new Surveys()
                                    {
                                        chatId = chat.id,
                                        surveyId = post.id,
                                        surveyKey = post.key,
                                        surveyType = "post-survey",
                                        value = post.value
                                    });
                                }
                                db.SaveChanges();
                                tx.WriteLine("Guardado exitosamente");
                            }
                        }
                    }

                }
            }
            catch (Exception ex1)
            {
                tx.WriteLine("Ocurrio un problema: " + ex1.Message.ToString());
            }
            tx.Close();
        }
    }

    public class Visitor
    {
        public string id { get; set; }
        public string name { get; set; }
        public string ip { get; set; }
        public string city { get; set; }
        public string region { get; set; }
        public string country { get; set; }
        public string country_code { get; set; }
        public string timezone { get; set; }
        public string user_agent { get; set; }
    }

    public class Agent
    {
        public string display_name { get; set; }
        public string email { get; set; }
        public string ip { get; set; }
    }

    public class Greeting
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class Message
    {
        public string author_name { get; set; }
        public string text { get; set; }
        public string message_json { get; set; }
        public string date { get; set; }
        public int timestamp { get; set; }
        public string agent_id { get; set; }
        public string user_type { get; set; }
        public string type { get; set; }
        public bool welcome_message { get; set; }
    }

    public class PrechatSurvey
    {
        public string key { get; set; }
        public string value { get; set; }
        public string id { get; set; }
    }

    public class Event
    {
        public string author_name { get; set; }
        public string text { get; set; }
        public string message_json { get; set; }
        public string date { get; set; }
        public int timestamp { get; set; }
        public string agent_id { get; set; }
        public string user_type { get; set; }
        public string type { get; set; }
        public bool welcome_message { get; set; }
        public string event_type { get; set; }
    }

    public class PostchatSurvey
    {
        public string key { get; set; }
        public string value { get; set; }
        public string id { get; set; }
    }

    public class Chat
    {
        public string type { get; set; }
        public string id { get; set; }
        public List<object> tickets { get; set; }
        public string visitor_name { get; set; }
        public string visitor_id { get; set; }
        public List<Operator> operators { get; set; }
        public string visitor_ip { get; set; }
        public Visitor visitor { get; set; }
        public List<Agent> agents { get; set; }
        public List<object> supervisors { get; set; }
        public string rate { get; set; }
        public int duration { get; set; }
        public string chat_start_url { get; set; }
        public string referrer { get; set; }
        public List<int> group { get; set; }
        public string started { get; set; }
        public List<object> custom_variables { get; set; }
        public bool pending { get; set; }
        public List<object> tags { get; set; }
        public string timezone { get; set; }
        public Greeting greeting { get; set; }
        public List<Message> messages { get; set; }
        public List<PrechatSurvey> prechat_survey { get; set; }
        public List<Event> events { get; set; }
        public string engagement { get; set; }
        public int started_timestamp { get; set; }
        public int ended_timestamp { get; set; }
        public string ended { get; set; }
        public List<PostchatSurvey> postchat_survey { get; set; }
    }


    public class Operator
    {
        public string display_name { get; set; }
        public string email { get; set; }
        public string ip { get; set; }
    }

    public class RootObject
    {
        public List<Chat> chats { get; set; }
        public int total { get; set; }
        public int pages { get; set; }
    }
}


