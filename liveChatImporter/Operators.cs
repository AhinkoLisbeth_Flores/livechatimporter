﻿namespace liveChatImporter
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("livechat.Operators")]
    public partial class Operators
    {
        public int id { get; set; }

        [Required]
        [StringLength(150)]
        public string chatId { get; set; }

        [StringLength(250)]
        public string operatorName { get; set; }

        [StringLength(150)]
        public string operatorEmail { get; set; }

        [StringLength(150)]
        public string operatorIp { get; set; }
    }
}