namespace liveChatImporter
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("livechat.Messages")]
    public partial class Messages
    {
        [Required]
        [StringLength(150)]
        public string chatId { get; set; }

        [Key]
        public int messageId { get; set; }

        [Required]
        [StringLength(150)]
        public string authorName { get; set; }

        [Required]
        public string text { get; set; }

        public DateTime date { get; set; }

        public int timestamp { get; set; }

        [StringLength(150)]
        public string agentId { get; set; }

        [Required]
        [StringLength(50)]
        public string userType { get; set; }
    }
}
