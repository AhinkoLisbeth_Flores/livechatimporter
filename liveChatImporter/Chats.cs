namespace liveChatImporter
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("livechat.Chats")]
    public partial class Chats
    {
        public int id { get; set; }

        [Required]
        [StringLength(150)]
        public string chatId { get; set; }

        [Required]
        [StringLength(250)]
        public string visitorName { get; set; }

        [Required]
        [StringLength(150)]
        public string visitorId { get; set; }

        [StringLength(150)]
        public string visitorIp { get; set; }

        [StringLength(100)]
        public string visitorCity { get; set; }

        [StringLength(100)]
        public string visitorCountry { get; set; }

        public string visitorBrowserInfo { get; set; }

        [StringLength(50)]
        public string rate { get; set; }

        public int duration { get; set; }

        [StringLength(150)]
        public string chatStartUrl { get; set; }

        public DateTime started { get; set; }

        public DateTime ended { get; set; }

    }
}
