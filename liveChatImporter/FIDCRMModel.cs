namespace liveChatImporter
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class FIDCRMModel : DbContext
    {
        public FIDCRMModel()
            : base("name=FIDCRMEntities")
        {
        }

        public virtual DbSet<Chats> Chats { get; set; }
        public virtual DbSet<Messages> Messages { get; set; }
        public virtual DbSet<Surveys> Surveys { get; set; }
        public virtual DbSet<Operators> Operators { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Chats>()
                .Property(e => e.chatId)
                .IsUnicode(false);

            modelBuilder.Entity<Chats>()
                .Property(e => e.visitorName)
                .IsUnicode(false);

            modelBuilder.Entity<Chats>()
                .Property(e => e.visitorId)
                .IsUnicode(false);

            modelBuilder.Entity<Chats>()
                .Property(e => e.visitorIp)
                .IsUnicode(false);

            modelBuilder.Entity<Chats>()
                .Property(e => e.visitorCity)
                .IsUnicode(false);

            modelBuilder.Entity<Chats>()
                .Property(e => e.visitorCountry)
                .IsUnicode(false);

            modelBuilder.Entity<Chats>()
                .Property(e => e.visitorBrowserInfo)
                .IsUnicode(false);

            modelBuilder.Entity<Chats>()
                .Property(e => e.rate)
                .IsUnicode(false);

            modelBuilder.Entity<Chats>()
                .Property(e => e.chatStartUrl)
                .IsUnicode(false);

            modelBuilder.Entity<Messages>()
                .Property(e => e.chatId)
                .IsUnicode(false);

            modelBuilder.Entity<Messages>()
                .Property(e => e.authorName)
                .IsUnicode(false);

            modelBuilder.Entity<Messages>()
                .Property(e => e.text)
                .IsUnicode(false);

            modelBuilder.Entity<Messages>()
                .Property(e => e.agentId)
                .IsUnicode(false);

            modelBuilder.Entity<Messages>()
                .Property(e => e.userType)
                .IsUnicode(false);

            modelBuilder.Entity<Surveys>()
                .Property(e => e.chatId)
                .IsUnicode(false);

            modelBuilder.Entity<Surveys>()
                .Property(e => e.surveyId)
                .IsUnicode(false);

            modelBuilder.Entity<Surveys>()
                .Property(e => e.surveyKey)
                .IsUnicode(false);

            modelBuilder.Entity<Surveys>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<Surveys>()
                .Property(e => e.surveyType)
                .IsUnicode(false);
        }
    }
}
