namespace liveChatImporter
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("livechat.Surveys")]
    public partial class Surveys
    {
        public int id { get; set; }

        [Required]
        [StringLength(150)]
        public string chatId { get; set; }

        [StringLength(150)]
        public string surveyId { get; set; }

        public string surveyKey { get; set; }

        public string value { get; set; }

        [Required]
        [StringLength(100)]
        public string surveyType { get; set; }
    }
}
